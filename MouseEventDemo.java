import java.awt.*;
import java.awt.event.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.*;


public class MouseEventDemo extends Frame implements MouseListener, MouseMotionListener
{
    TextField tf;
    TextField tf2;
    public MouseEventDemo(String title){
        super(title);
        tf = new TextField(60);
        tf2 = new TextField(60);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void launchFrame(){
        add(tf,BorderLayout.SOUTH);
        add(tf2,BorderLayout.NORTH);
        setSize(300,300);
        setVisible(true);
    }

    //Overidding method dari interface MouseListener
    public void mouseClicked(MouseEvent m){
        String msg = "Mouse Clicked";
        tf.setText(msg);
    }

    public void mouseEntered(MouseEvent m){
        String msg = "MOUSE MASUK";
        tf.setText(msg);
    }

    public void mouseExited(MouseEvent e){
        String msg = "MOUSE KELUAR";
        tf.setText(msg);
    }

    public void mouseReleased(MouseEvent e){

    }
    public void mousePressed(MouseEvent e){

    }

    //Overide dari interface MouseMotionListener
    public void mouseDragged(MouseEvent e){

    }
    public void mouseMoved(MouseEvent e){
        String msg = "Mouse Terdapat Pada Sumbu " + e.getX()+","+e.getY();
        tf2.setText(msg);
    }
   


    public static void main(String[] args){
        MouseEventDemo demo = new MouseEventDemo("Coba mouse event");
        demo.launchFrame();

    }
}
