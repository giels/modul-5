import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class DiscoLightsExample extends JFrame implements ActionListener
{
    private final String act_on = "Light ON";
    private final String act_off = "Light OFF";
    private final String act_Cycle = "Cycle Color";
    private final Color[] warna = new Color[]{
        Color.white,
        Color.green,
        Color.red,
        Color.yellow,
        Color.orange,
        Color.pink
    };

    public int currentColor = 0;
    private boolean isLightOn = false;
    public DiscoLightsExample(){
        setTitle("Disco Light Party Frame");
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton btnON = new JButton("Light ON");
        JButton btnColor = new JButton("Cycle Color");
        btnON.setActionCommand(act_on);
        btnColor.setActionCommand(act_Cycle);
        btnON.addActionListener(this);
        btnColor.addActionListener(this);
        getContentPane().add(btnON);
        getContentPane().add(btnColor);
        pack();
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e){
        String action = e.getActionCommand();
        System.err.println("Got action "+action);
        switch(action){
            case act_on:
            isLightOn = true;
            getContentPane().setBackground(warna[currentColor]);
            ((JButton) e.getSource()).setText("Light OFF");
            ((JButton) e.getSource()).setActionCommand(act_off);
            break;

            case act_off:
            isLightOn = false;
            getContentPane().setBackground(Color.BLACK);
            ((JButton) e.getSource()).setText("Light ON");
            ((JButton) e.getSource()).setActionCommand(act_on);
            break;

            case act_Cycle:
            if(isLightOn){
                getContentPane().setBackground(warna[++currentColor%warna.length]);
                break;
            }
        }
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
        
            @Override
            public void run() {
                new DiscoLightsExample();
            }
        });
    }}
